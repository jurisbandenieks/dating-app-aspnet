import { Component, OnInit } from "@angular/core";
import { HttpClient, HttpResponse } from "@angular/common/http";

@Component({
  selector: "app-value",
  templateUrl: "./value.component.html",
  styleUrls: ["./value.component.css"],
})
export class ValueComponent implements OnInit {
  values: any;

  constructor(private http: HttpClient) {}

  ngOnInit() {
    this.getValue();
  }

  getValue() {
    this.http.get("http://localhost:5000/api/Values").subscribe(
      (res: HttpResponse<any>) => {
        this.values = res;
      },
      (err: Error) => {
        console.log(err);
      }
    );
  }
}
